package com.bco4013.workshop1;

import java.util.Scanner;

public class ScannerClass {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String name;
		int age;
		
		Scanner reader = new Scanner(System.in);

		System.out.print("Hello, what is your name? ");
		name = reader.nextLine();
		
		System.out.print("Hello, what is your age? ");
		age = reader.nextInt();

		System.out.println("Nice to meet you " + name + ", you are " + age + "!");	
		
	}

}
