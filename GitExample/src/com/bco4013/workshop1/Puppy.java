package com.bco4013.workshop1;

public class Puppy
{
	   // is this default
	   int puppyAge;
	   String puppyName;
	   String puppyBreed;

	   // add more private variabkles
	   private String ownerName;
	   private String address;
	   
	   public Puppy(String name, String _ownName, String _address, String breed)
	   {
		   ownerName = _ownName;
		   address = _address;
		   puppyName = name;
		   puppyBreed = breed;
		   // This constructor has one parameter, name.
		   System.out.println("Passed Name is :" + name + " breed is: " + breed); 
	   }
	   
	   public String getPuppyName()
	   {
		   return puppyName;
	   }
	   
	   public void setAge( int age ){
	       puppyAge = age;
	   }

	   public int getAge( )
	   {
	       System.out.println("Puppy's age is :" + puppyAge ); 
	       return puppyAge;
	   }
	   public static void main(String []args)
	   {
	      /* Object creation */
	      Puppy myPuppy = new Puppy( "tommy" , "Laura", "MK", "Lab");

	      /* Call class method to set puppy's age */
	      myPuppy.setAge( 2 );

	      /* Call another class method to get puppy's age */
	      myPuppy.getAge( );

	      /* You can access instance variable as follows as well */
	      System.out.println("Puppy name :" + myPuppy.getPuppyName() + " age, " + myPuppy.puppyAge ); 
	   }
	}

