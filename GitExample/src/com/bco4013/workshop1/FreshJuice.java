package com.bco4013.workshop1;

public class FreshJuice 
{
	/*
	 * enum enables a variable to be set for one or more constants. use capitalise letters
	 * ex: days of the week, planets in solar system
	 */
	
	enum FreshJuiceSize{SMALL, MEDIUM, LARGE };
	enum FreshJuiceType{ORANGE, MANGO, APPLE};
	
	double juice_price;
	
	FreshJuiceSize jsize;
	FreshJuiceType jtype;
	
	
	Boolean is_mixed;
	Boolean is_sugar_free;
	
	public double get_juice_price(FreshJuiceSize _jsize, int qty)
	{
		if(_jsize == FreshJuiceSize.SMALL)
			juice_price = qty * 10;
		else if(_jsize == FreshJuiceSize.MEDIUM)
			juice_price = qty * 25;
		else 
			juice_price = qty * 35;
		
		return juice_price;
	}
}

