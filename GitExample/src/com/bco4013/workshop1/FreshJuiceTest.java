package com.bco4013.workshop1;

import com.bco4013.workshop1.FreshJuice.FreshJuiceType;

public class FreshJuiceTest 
{
	   public static void main(String args[])
	   {
	      FreshJuice juice = new FreshJuice();
	      // cannot do this if params in FreshJuice were private
	      juice.jsize = FreshJuice.FreshJuiceSize.MEDIUM ;
	      juice.jtype = FreshJuice.FreshJuiceType.MANGO;
	      
	      System.out.println("Size: " + juice.jsize);
	      System.out.println("Type: " + juice.jtype);
	      System.out.println("Price: " + juice.get_juice_price(juice.jsize, 2));
	      
	   }

}

