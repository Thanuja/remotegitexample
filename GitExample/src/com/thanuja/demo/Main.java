/**
 * Naming package names;
 * it is likely that many programmers will use the same name for different types
 * eg. Custom Rectangle class, and Java has inbuilt Rectangle class in awt package
 * 
 * Fully qualified names of the class includes packagename.class
 * 	graphics.Rectangle
 * 	Java.awt.Rectangle
 * 
 * What if two programmers use same names?? Soln.; Conventions
 * https://docs.oracle.com/javase/tutorial/java/package/namingpkgs.html
 * 
 * using domain names;
 * e.g.,
 * 	com.example.region.mypackage
 * 	org.example.mypackage
 * 	ac.cardiffmet.username.mypackage
 * 
 * Conflicting domains e.g. int, (conflicts with built-in types), use _; e,g. int_
 * Packages in Java begin with Java, or Javax
 * 
 */

package com.thanuja.demo;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Hello Git tutorial");
		System.out.println("Hello Git");
		
		System.out.println("Test 1");
	}

}
