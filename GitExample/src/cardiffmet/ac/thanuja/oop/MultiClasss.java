package cardiffmet.ac.thanuja.oop;

/**
 * 
 * 
 * @author thanu
 *
 * access modifiers
 * =================
 * 
 * public:
 * 		OK; all
 * 
 * default:
 * 		OK: same class, same package sub-class, same package non-subclass
 * 		not OK: different package non-subclass, diff. package sub class
 * 
 * private:
 * 		ok: same class
 * 		not ok; all other
 * 
 * protected: 
 * 		OK: all other
 * 		no OK: diff. package non-sub class
 * 
 */

/*
 * Class is a blue print from which the objects are created.
 * 
 */
public class MultiClasss {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String msg = "Local varible in main()";
		System.out.println(msg);
		
		// why the errors
		System.out.println(Data.txt);
		Data.greetings();
		Draw.line();
		
	}

}
