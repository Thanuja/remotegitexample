package cardiffmet.ac.thanuja.oop;

/*
 * The method in the super class may be explicitly addressed using its class name 
 * and dot notation. For example, SuperClass.run().
 * 
 * try-catch statement in a method within a super class does not catch exceptions 
 * that occur in a sub class 
 * 
 */

public class MountainBike extends Bicycle 
{

	public MountainBike() 
	{
		// TODO Auto-generated constructor stub
	}
}
