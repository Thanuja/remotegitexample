package cardiffmet.ac.thanuja.oop;

public class SubClass extends SuperClass {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		hello();
		SuperClass.hello();
		try
		{
			// sub class exceptions must be handled in sub class
			echo(args[0]);
		}
		catch(ArrayIndexOutOfBoundsException e)
		{
			System.out.println("Need an arugment");
		}
		
	}

	public static void hello()
	{
		System.out.println("Hello from sub class");
	}
	
}
