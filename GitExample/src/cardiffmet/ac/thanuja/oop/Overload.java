package cardiffmet.ac.thanuja.oop;

public class Overload {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println(write(4));
		System.out.println(write("Thanuja"));
		System.out.println(write(4,4));
	}

	public static String write(int num)
	{
		return ("The integer passed is: " + num);
	}
	
	public static String write(String str)
	{
		return ("The string passed is: " + str);
	}
	
	public static String write(int num1, int num2)
	{
		return ("The Sum is: " + (num1 + num2));
	}
	
	
}
