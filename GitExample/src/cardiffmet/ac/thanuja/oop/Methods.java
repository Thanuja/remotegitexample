package cardiffmet.ac.thanuja.oop;

public class Methods {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Message from main()");
		sub();
		Methods.sub();
		//non_static_sub(); // this is not possible!
		
		//Need an object of the class to call the non-static method...
		Methods m1 = new Methods();
		m1.non_static_sub();
		
	}
	
	// doesnt have to public, as it is not called from other classes..
	static void sub()
	{
		System.out.println("Message from sub()");
	}
	
	void non_static_sub()
	{
		System.out.println("Message from non-static sub()");
	}

}
