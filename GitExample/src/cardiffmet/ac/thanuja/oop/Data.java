package cardiffmet.ac.thanuja.oop;

public class Data {

	// discuss what happens when these access modifiers are removed..
	public final static String txt = "Global, final static public variable in Data class";
	
	public static void greetings()
	{
		System.out.println("greetings() is a public static method in Data class");
	}
	
}
