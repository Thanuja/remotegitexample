package cardiffmet.ac.thanuja.oop;

public class Scope {

	/**
	 * @param args
	 */
	
	//String text = "This is a class variable string";			//without static
	//static String text = "This is a class variable string"; 	// with static
	
	/*
	 * final keyword:
	 * --------------
	 * only initialized once, at the time of declaration or later assignment, but once..
	 * 
	 * final vs constants
	 * -----------------
	 * In final, values are not known at the compile time. 
	 * Constants are known at the compile time..
	 * 
	 * final methood:
	 * =============
	 * cannot be overridden in a subclass >> security
	 * 
	 * final classess;
	 * ===============
	 * cannot be subclassed >> security
	 * 
	 */
	final static String text = "This is a class variable string"; 
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String text = "Local variable in the main()";
		System.out.println(Scope.text);		// this will not work without static
		//Scope.text = "Changing..";			// this is not allowed as it is declared as final
		System.out.println(text);
		
		sub();
		
	}
	
	public static void sub()
	{
		String text = "Local variable in the sub()";
		System.out.println(text);
	}

}
