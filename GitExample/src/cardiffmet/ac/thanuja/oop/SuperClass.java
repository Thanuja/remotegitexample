package cardiffmet.ac.thanuja.oop;

public class SuperClass {

	
	public static void hello()
	{
		System.out.println("Hello from the super class");
		
	}
	
	public static void echo(String arg)
	{
		try
		{
			System.out.println("Passed argument is ; " + arg);
		}
		catch(Exception e)
		{
			System.out.println("No argument passed");
		}
	}
	
}
