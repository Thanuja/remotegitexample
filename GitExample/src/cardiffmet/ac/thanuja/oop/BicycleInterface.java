package cardiffmet.ac.thanuja.oop;

/**
 * An interface is a group of related methods with EMPTY bodies, provides an interface between an
 * object and real world
 * Interfaces form a contract between the class and the outside world, 
 * and this contract is enforced at build time by the compiler. 
 * If your class claims to implement an interface, 
 * ALL methods defined by that interface must appear in its source code
 * 
 * 
 * @author thanu
 *
 */
public interface BicycleInterface {
	
//  wheel revolutions per minute
    void changeCadence(int newValue);

    void changeGear(int newValue);

    void speedUp(int increment);

    void applyBrakes(int decrement);

}
