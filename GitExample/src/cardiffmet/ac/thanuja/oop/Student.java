package cardiffmet.ac.thanuja.oop;

import cardiffmet.ac.thanuja.basics.Person;

public class Student extends Person {

	private String student_number;
	private String course_title;
	
	public Student(){super();}
	
	public Student(String fname, String lname, int age, String stno, String course)
	{
		super(fname, lname, age);
		student_number = stno;
		course_title = course;
	}
	
	
	public String getStudentNumber(){return student_number;}
	public void setStudentNumber(String student){student_number = student;}
	
	public String getCourseTitle(){return course_title;}
	public void setCourseTitle(String title){course_title = title;}
	
	
}
