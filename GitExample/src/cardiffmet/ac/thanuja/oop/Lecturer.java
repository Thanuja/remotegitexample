package cardiffmet.ac.thanuja.oop;

import cardiffmet.ac.thanuja.basics.Person;

public class Lecturer extends Person{

	private String staff_number;
	private String job_title;
	
	public Lecturer()
	{
		super();
	}

	public Lecturer(String fname, String lname, int age, String sfno, String job)
	{
		super(fname, lname, age);
		staff_number = sfno;
		job_title = job;
	}
	
	
	
	public String getStaffNumber(){return staff_number;}
	public void setStaffNumber(String staff){staff_number = staff;}
	
	public String getJobTitle(){return job_title;}
	public void setJobTitle(String title){job_title = title;}
	
	
	
}
