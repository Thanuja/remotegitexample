package cardiffmet.ac.thanuja.basics;
import java.util.concurrent.TimeUnit;

public class Branching {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int yourage = 25;
		if(yourage < 30)
			System.out.print(" You are younger than 30!!\n");
		
		
		int yourscore = 25;
		int myscore = 100;
		
		if (myscore < yourscore)
			System.out.println(" You beat me!! weldone...");
		else
			System.out.println(" Ha ha, I beat you by " +(myscore - yourscore));
		
		int testscore = 76;
		char grade;
		
		if(testscore >= 90)
			grade = 'A';
		else if (testscore >= 80)
			grade = 'B';
		else if (testscore >= 70)
			grade = 'C';
		else if (testscore >= 60)
			grade = 'D';
		else 
			grade = 'F';
		
		System.out.println("Your grade is : " + grade);
		
		// Switches:
		
		int month = 8;
		String month_string;
		switch (month) {
		case 1:
			month_string = "January";
			break;

		case 8:
			month_string = "August";
			break;
		default:
			month_string = "Other";
			break;
		}
		
		System.out.println("Your month is : " + month_string);
		
		
		// if and if, Another if
		int number = 5;
		if ((number > 4) && (number < 6))
			System.out.println("You have chosen number : 5");
		// Ammend the code to say, "You must have chosen 5 or above 10" 
		
		// IF else
		
		int hours = 15;
		if (hours < 12)
			System.out.println("Good Morning");
		else if (hours < 18)
			System.out.println("Good Afternoon");
		else
			System.out.println("Good evening");
		
		// Exercise: 
		/*
		 * tell the user that a number higher than 24 has been assigned to the variable 
		 * and that it is an invalid number
		 */
		// Change the output string to show the value of time
		// 'Good Morning. The time is 10:00 o'clock'
		
		long currentTime_millisecs = System.currentTimeMillis(); // current time in milliseconds
		System.out.println(currentTime_millisecs);
		int seconds = (int) (currentTime_millisecs / 1000) % 60 ;
		int minutes = (int) ((currentTime_millisecs / (1000*60)) % 60);
		hours   = (int) ((currentTime_millisecs / (1000*60*60)) % 24);
		System.out.println("Current time = " + hours +":"+ minutes + ":" + seconds);
		//conversion using timeunits
		
		String formattedTime = String.format("%d min, %d sec", 
			    TimeUnit.MILLISECONDS.toMinutes(currentTime_millisecs),
			    TimeUnit.MILLISECONDS.toSeconds(currentTime_millisecs) - 
			    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(currentTime_millisecs))
			);
		System.out.println("Current time (Formatted) = " + formattedTime);
			
		
		
		
		System.out.print("End of program!"); // No new line is added without println();
	}

}
