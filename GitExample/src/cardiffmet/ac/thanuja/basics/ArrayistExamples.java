package cardiffmet.ac.thanuja.basics;


import java.util.*;

/**
 * 
 * @author thanu
 *	Arraylists are good when we dont know, about the size of the array, 
 *	A collection, 
 */


public class ArrayistExamples {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ArrayList<String> namelist = new ArrayList<String>();
		namelist.add("Thanuja");
		
		String myname = namelist.get(0);
		for (int i = 0; i < namelist.size(); i++)
		{
			if(namelist.get(i).startsWith("T"))
				System.out.println("Name " + (i+1) + " starts with T");
		}
		
		if(namelist.contains("Benson"))
			System.out.println("Benson is a valid name"); 
		
		
		namelist.clear();
		
		namelist.add("Alpha");
		namelist.add("Delta");
		namelist.add("Charlie");
		
		System.out.println("List: " + namelist);
		
		//replacing
		System.out.println("Replacing " + namelist.get(1));
		namelist.set(1, "Bravo");
		
		System.out.println("New item " + namelist.get(1));
		
		
		// study lambda expressions!!! Java 8
		
	}

}
