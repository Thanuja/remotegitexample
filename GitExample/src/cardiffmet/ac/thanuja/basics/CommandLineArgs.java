package cardiffmet.ac.thanuja.basics;

import java.awt.font.NumericShaper;

/**
 * 
 * @author thanu
 * 
 * Program arguments vs VM arguments
 * --------------------------------
 * Parameters passed to the program via main(String [] args)
 * 
 * VM arguments; parameters to the Java Interpreter; Instruct VM to do something. control heap size
 *
 */

public class CommandLineArgs {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Echo command line arguments");
		int firstArg;
		
		if(args.length > 0)
		{
			try
			{
				firstArg = Integer.parseInt(args[0]);
			}
			catch (NumberFormatException e) {
				// TODO: handle exception
				System.err.println("Argument" + args[0] + " must be an integer.");
		        System.exit(1);
				
			}
		}
		
		// for each loop
		for(String s: args)
			System.out.println(s);
		
		
		
	}

}
