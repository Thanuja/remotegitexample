package cardiffmet.ac.thanuja.basics;

import java.util.Scanner;

/**
 * 
 * @author thanu
 *	This is one simple way of doing this. 
 *	Use a calculator class to store, operator, operand, and accumulator
 *	Like a real calculator: 
 *		ask a number
 *		operator
 *		next number
 *		show result
 *
 *	
 */

public class CalculatorExample {

	/**
	 * @param args
	 */
	enum operation{ADD, SUB, MUL, DIV};
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int valuea = 0, valueb = 0;
		int menu = -1;
		int result = 0;
			
		Scanner input = new Scanner(System.in);
		System.out.println("*** My Calculator ***");
		System.out.println("\n 1. Addition, \n 2. Subtraction, \n 3. Multiplication, \n 4. Division, \n 5. Exit \n Please choose an option: ");
		
		while(true)
		{
			System.out.println("Enter a number: ");
			valuea = input.nextInt();
			
			System.out.println("Enter a number: ");
			valueb = input.nextInt();
			
			System.out.println("\n Enter the operation \n 1. Addition, \n 2. Subtraction, \n 3. Multiplication, \n 4. Division, \n 5. Exit \n Please choose an option: ");
			menu = input.nextInt();
			
			switch (menu) {
			case 1:
				result = valuea + valueb;
				System.out.println("Result =  " + result);
				break;

			case 2:
				result = valuea - valueb;
				System.out.println("Result =  " + result);
				break;
				
			case 3:
				result = valuea * valueb;
				System.out.println("Result =  " + result);
				break;
				
			case 4:
				result = valuea / valueb;
				System.out.println("Result =  " + result);
				break;
				
			case 5:
				System.exit(1);
				break;
				
			default:
				System.out.println("Invalid operation!");
				break;
			}
		}
		
	}

}
