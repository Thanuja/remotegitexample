package cardiffmet.ac.thanuja.basics;

/**
 * 
 * @author thanu
 *	Data Types:
 *	8 Primitive data types: Byte, Short, Int, Long, Float, Double, Char, Boolean
 *	String; is a character array
 *
 *	Statically typed laguage; Java, C++ as opposed to Python
 *		A variable has to be declared with Name and Type before usage
 *	Primitive Data types:
 *		defined by the language and use reserved keywords
 *	
 *------------------------------------------------
 *
 *	byte	; 8 bit signed two's complement; range -128 to +127
 *	short	: 16-bit, signed two's complement; range -32,768 to 32,767
 *	int		: 32-bit signed two's complement; support unsigned int for Java SE> 8
 *	long	: 64-bit, signed two's complement; support unsigned long for Java SE > 8
 *	float	: single-precision, 32-bit IEEE 754 format
 *	double	: double-precision 64-bit IEEE 754 floating point
 *	boolean	: 1 bit of information
 *	char	: 16-bit unicode character
 *	
 *	String (Java.lang.String) is a special class that support array of characters (not a primitive)
 *	
 *	Use of uninitialised local variables >> compile time error >> bad practice
 *	
 *
 */



public class DataTypesVariables {

	/**
	 * @param args
	 */
	static String name; // static is needed as main() is a static method!!!
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		name = "Thanuja";
		String lecturer_name = "Thanuja";
		int lecturer_age=25;
		
		System.out.println("Hello, my name is " + lecturer_name);
		System.out.println("I am " + lecturer_age + " years old");
		
		// change content of variables
		lecturer_name = "Prof. X";
		lecturer_age = 41;
		System.out.println("Hello, my name is " + lecturer_name);
		System.out.println("I am " + lecturer_age + " years old");
		
		
		// Literals: Statements, no computations needed
		boolean result = true;
		char capitalC = 'C';
		byte b = 100;
		short s = 10000;
		int i = 100000;
		
		/*
		 * Integer literals
		 * note: 0x and 0b for hex and binary, respectively
		 */
		// The number 26, in decimal
		int decVal = 26;
		//  The number 26, in hexadecimal
		int hexVal = 0x1a;
		// The number 26, in binary
		int binVal = 0b11010;
		
		/*
		 * floating point literals
		 */
		double d1 = 123.4;
		// same value as d1, but in scientific notation
		double d2 = 1.234e2;
		float f1  = 123.4f;	//f-32 bits, d- for 64-bits (default)
		
		/*
		 * Character and String literals; represent Unicode characters; system/IDE should allow
		 * 		Unicode: an encoding standard for characters, assign a number for each character
		 * 
		 * '' for char
		 * "" for strings
		 * Escape seuqneces: \b, \n, \t, \", \', \\, \r, \f
		 * 
		 */
		
		/*
		 * Using _ in numeric literals; Java SE > 7
		 * Allows you to group digits, >> better representation, ONLY between digits
		 * 
		 */
		long creditCardNumber = 1234_5678_9012_3456L;	// Long type
		long socialSecurityNumber = 999_99_9999L;
		float pi =  3.14_15F;	// 32-bit Float
		long hexBytes = 0xFF_EC_DE_5E;
		long hexWords = 0xCAFE_BABE;
		long maxLong = 0x7fff_ffff_ffff_ffffL;
		byte nybbles = 0b0010_0101;
		long bytes = 0b11010010_01101001_10010100_10010010;
		
		// Invalid literals
		// Invalid: cannot put underscores
		// adjacent to a decimal point
		////float pi1 = 3_.1415F; 
		// Invalid: cannot put underscores 
		// adjacent to a decimal point
		/// float pi2 = 3._1415F;
		// Invalid: cannot put underscores 
		// prior to an L suffix
		///long socialSecurityNumber1 = 999_99_9999_L;

		// OK (decimal literal)
		int x1 = 5_2;
		// Invalid: cannot put underscores
		// At the end of a literal
		///int x2 = 52_;
		// OK (decimal literal)
		int x3 = 5_______2;
		
		// Invalid: cannot put underscores
		// in the 0x radix prefix
		//int x4 = 0_x52;
		// Invalid: cannot put underscores
		// at the beginning of a number
		//int x5 = 0x_52;
		// OK (hexadecimal literal)
		int x6 = 0x5_2; 
		// Invalid: cannot put underscores
		// at the end of a number
		//int x7 = 0x52_;
		
	}

}
