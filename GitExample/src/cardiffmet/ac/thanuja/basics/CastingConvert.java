package cardiffmet.ac.thanuja.basics;

/**
 * 
 * @author thanu
 *	
 *	Casting, converting data types
 *
 */

public class CastingConvert {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		float daysFloat = 365.25f;
		String weeksStrings = "52";
		
		int daysInt = (int) daysFloat;
		int weeksInt = Integer.parseInt(weeksStrings);
		int week = daysInt / weeksInt;
		System.out.println("Days per week: " + week);
		
	}

}
