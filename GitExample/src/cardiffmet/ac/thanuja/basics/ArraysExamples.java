package cardiffmet.ac.thanuja.basics;

public class ArraysExamples {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int[] scores = {7, 8, 10, 12, 7};
		float averageScores = 0;
		if(scores.length > 0)
		{
			int scoreSum = 0;
			for(int score: scores)
			{
				scoreSum += score;
				System.out.println("current score: " + score);
			}
			System.out.println("Average scores: " + (float)scoreSum/scores.length);
		}
		
		String[] names = {"Cath", "Tom", "Ana", "Giles"};
		int ii = 0;
		while(ii < names.length)
		{
			System.out.println("names: " + names[ii]);
			ii++;
		}
		
		ii = 0;
		do
		{
			System.out.println("names: " + names[ii]);
			ii++;
		}
		while(ii < names.length);
		
		// 2D arrays
		boolean[][] points = new boolean[5][20]; // 5 rows, 20 columns
		// default values zeros added initially
		points[0][5] = true;		
		points[1][6] = true;
		points[2][7] = true;
		points[3][8] = true;
		points[4][9] = true;

		for(ii = 0; ii < points.length; ii++)
		{
			System.out.print("\n");
			for(int jj = 0; jj < points[ii].length; jj++)
			{
				char mark = points[ii][jj] ? 'X' : '-';
				System.out.print(mark);
			}
		}
		
		
		String[] str = {"Much", "more", "Java"};
		int[] num = new int[3];
		
		num[0] = 100;
		num[1] = 120;
		
		//access elements
		System.out.println("String array length: " + str.length);
		System.out.println("Integer array length: " + num.length);
		System.out.println("numbers: " + num[0] + " , " + num[1] + " , " + num[2]);
		System.out.println("Strings: " + str[0] + " , " + str[1] + " , " + str[2]);
		
		
		String[] childnames = {"April", "James", "Mary", "Simon"};
		int[] childages = {5, 6, 9, 11};
		
		int childnumber = 1;
		do
		{
			System.out.println("Child number " + childnumber+ " is called " + childnames[childnumber-1] + " and is " + childages[childnumber -1] + " years old");
			childnumber++;
		}
		while(childnumber <= childages.length);
		
		
		// vending machines
		int[] vend1_qrt1 = {4200, 4800, 5000};
		int[] vend1_qrt2 = {5200, 5800, 5000};
		int[] vend1_qrt3 = {4600, 4900, 5800};		
		int[] vend1_qrt4 = {5000, 5100, 6100};
		
		int[] vend2_qrt1 = {4200, 4800, 5000};
		int[] vend2_qrt2 = {5200, 5800, 5000};
		int[] vend2_qrt3 = {4600, 4900, 5800};		
		int[] vend2_qrt4 = {5000, 5100, 6100};
		
		// allocate memory
		int[] sum = new int[12]; // total for each month
		
		int total = 0;
		for(int i=0; i<vend1_qrt1.length; i++)
		{
			sum[i] = vend1_qrt1[i] + vend2_qrt1[i];
			sum[i+3] = vend1_qrt2[i] + vend2_qrt2[i];
			sum[i+6] = vend1_qrt3[i] + vend2_qrt3[i];
			sum[i+9] = vend1_qrt4[i] + vend2_qrt4[i];
			
		}
		
		for(int i =0; i < sum.length; i++)
		{
			System.out.println("Month: " + (i+1) + " , Sales: " + sum[i]);
			total += sum[i];
		}
		
		System.out.println("Total sales for the year: " + total);
		
		// reverse:
		char[] word1 = {'d', 'e', 's', 's', 'e', 'r', 't', 's'};
		char[] word2 = {'s', 'p', 'o', 'o', 'n', 's'};
		char[] word3 = {'d', 'e', 'l', 'i', 'v', 'e', 'r'};
		
		System.out.println(word1); // this prints the whole word, no need of a loop
		for (char c: word1)
			System.out.print(c);
		System.out.print("\n");
		
		for(int i = word1.length-1; i>=0; i--)
			System.out.print(word1[i]);
		
		System.out.print("\n");
		
		
		// Copy arrays from one to another;
		char[] copyFrom = { 'd', 'e', 'c', 'a', 'f', 'f', 'e', 'i', 'n', 'a', 't', 'e', 'd' };
	    char[] copyTo = new char[7];

	    System.arraycopy(copyFrom, 2, copyTo, 0, 7);
	    System.out.println(new String(copyTo));
		
		// array manipulations (common tasks, such as copying, sorting and searching arrays) in the java.util.Arrays class
	    
	    // Here no need to create the new/destination array, it is returned..
	    char[] copyTo2 = java.util.Arrays.copyOfRange(copyFrom, 2, 9);
		
	    /*
	     * Some important methods in Arrays class
	     * 1. copyofrange
	     * 2. binarysearch -> returns the index of searched element
	     * 3. equals -> compare two arrays
	     * 4. sort -> sequential sort, or parallelsort in Java SE > 8
	     */
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

}
