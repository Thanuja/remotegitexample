package cardiffmet.ac.thanuja.basics;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/*
 * More JUNIT examples are found in http://www.vogella.com/tutorials/JUnit/article.html 
 * 
 */



public class PersonTest {

	// Other annotations
	
	/*
	 * This is executed before each test. Setup test environment
	 */
	@Before
	public void beforeEachTest()
	{
		
	}
	
	/*
	 * This is executed after each Test. clean up environment. delete temporary data structures etc.
	 */
	@After
	public void afterEachTest()
	{
		
	}
	
	/*
	 * Executed once. Perform time intensive tasks. E.g., setup database connection, connect to a remote server
	 */
	@BeforeClass
	public static void BeforeClassOnce()
	{
		
	}
	
	/*
	 * Disable a certain test case. Useful when the test case is too long, time consuming, or not updated to reflect the changes in the actual code
	 */
	@Ignore("Why disabled")
	public void disabledTestCase()
	{
		
	}
	/*
	 * Executed once, after all test cases. e.g., disconnect from database, remote server connection
	 */
	@AfterClass
	public static void AfterClassOnce()
	{
		
	}
	
	
	/*
	 * Test case is failed if the completion time > 100 milliseconds
	 */
	@Test(timeout=100)
	public void testPersonStringInt() {
		//fail("Not yet implemented");
		Person p1 = new Person("Thanuja", "Mallikarachchi", 21);
		String last_name = p1.getLastName();
		
		assertNotSame("Age is not as same as 21", 21, p1.getAge());
	}

	@Test
	public void testPersonStringStringInt() {
		//fail("Not yet implemented");
		Person p1 = new Person("Thanuja", "Mallikarachchi", 21);
		String last_name = p1.getLastName();
		assertSame("Age should be 21", 21, p1.getAge());
		
	}

	@Test
	public void testGetLastName() {
		//fail("Not yet implemented");
		Person p1 = new Person("Thanuja", "Mallikarachchi", 21);
		String last_name = p1.getLastName();
		assertEquals("Last name should be Mallikarachchi", "Mallikarachchi", last_name);
	}

	/*
	 * Fails if the function doesnt throw the named exception!!!
	 */
	@Test (expected = Exception.class)
	public void testSetLastName() {
		
		
		Person p1 = new Person("Thanuja", "Mallikarachchi", 21);
		String last_name = p1.getLastName();
		assertNotNull("Last name is NULL failure", last_name);
		
		
		
		/*
		fail("Not yet implemented"); // causes the test method to fail explicitly
		
		boolean test = true;
		int expectedValue = 5;
		int value = 10;
		int value1 = 0;
		int value2 = 0;
		
		// sample assertion methods
		assertTrue("message", test); // This will fail, if the variable test is false
		assertFalse("message", test); // This will fail if the variable test is not false (true)
		
		assertEquals("message", expectedValue, value); // fails if the values are not equal 
		assertNotEquals("message", value1, value2); // fails if the values are equal to each other
		// The above two uses equal() method. i.e., can compare objects
		// Objects can become equal if they are in the same state. But they are NOT ==. An object is == only to itself.
		
		assertNull("message", value); // fails if the value is NOT NULL
		assertNotNull("message", value) ; // fails if the value is NULL
		
		assertSame(expectedValue, value);
		assertSame("message", expectedValue, value);
		assertNotSame(value1, value2);
		assertNotSame("message", value1, value2);
		// These (above) use == compare values. DO NOT USE these for object comparison
		*/
		
		
	}

	
	
	
	
}
