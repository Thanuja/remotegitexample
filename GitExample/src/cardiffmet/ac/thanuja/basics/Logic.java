package cardiffmet.ac.thanuja.basics;

public class Logic {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		boolean yes = true;
		boolean no = false;
		
		System.out.println("Both YesYes True: " + (yes && yes));
		System.out.println("Both YesNo True: " + (yes && no));
		
		System.out.println("Eithere YesYes True: " + (yes || yes));
		System.out.println("Eithere YesNo True: " + (yes || no));
		System.out.println("Eithere NoNO True: " + (no || no));
		
		System.out.println("Original Yes value: " + yes);
		System.out.println("Inverse Yes value: " + !yes);
		
	}

}
