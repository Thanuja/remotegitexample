package cardiffmet.ac.thanuja.basics;

public class MarsWeather {

	/**
	 * @param args
	 * 
	 * Air Pressure units Pascal (Pa)
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String header = "\n\t MARS ROVER ENVIRONMENT MONITORING PROGRAM DAY 3 FORCAST: \n";
		String forecast = "\t1125\t\t-22C\t-79C\t900pa\n";
		
		header += "\n\tSol\tHigh\tLow\tPressure\n";
		header += "\t---\t\t----\t---\t-------\n";
		forecast += "\t1126\t\t-21C\t-79C\t901pa\n";
		forecast += "\t1127\t\t-30C\t-80C\t910pa\n";
		
		System.out.println(header + forecast);
	}

}
