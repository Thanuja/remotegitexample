package cardiffmet.ac.thanuja.basics;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ReadString {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Scanner is space seperated. Consider each word as tokens
		
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader bufreader = new BufferedReader(isr);
		
		String input = "";
		
		int count= 0;
		while(count < 3)
		{
			System.out.println("Enter book title: ");
			try {
				input = bufreader.readLine();
				//bufreader.close(); // cannot close it here..
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("Error has occured");
			}
			System.out.println("Thank you for reading: " + input);
			
			count++;
		}
		
		try {
			bufreader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
		
	}

}
