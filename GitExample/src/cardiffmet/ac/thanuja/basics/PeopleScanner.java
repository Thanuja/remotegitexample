package cardiffmet.ac.thanuja.basics;

import java.util.ArrayList;
import java.util.Scanner;

public class PeopleScanner {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);
		String yourname;
		int yourage;
		
		ArrayList<Person> people = new ArrayList<Person>();
		
		for(int i=0; i<4 ; i++)
		{
			System.out.println("Enter person " + (i+1) + " details");
			System.out.println("==================================");
			
			System.out.println("Your name?: ");
			yourname = input.next();
			
			System.out.println("Your age?: ");
			yourage = input.nextInt();
			
			Person individual = new Person();
			individual.setName(yourname);
			individual.setAge(yourage);
			//System.out.println("Hello, " + yourname + " you are " + yourage + " years old!");
			
			people.add(individual);
			
		}
		input.close();
		
		int total_ages = 0;
		for(Person p:people)
		{
			total_ages += p.getAge();
			System.out.println("Hello, " + p.getName() + " you are " + p.getAge() + " years old!");
		}
		System.out.println("Average age: " + total_ages/people.size());	
		
	}

}
