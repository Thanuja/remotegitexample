package cardiffmet.ac.thanuja.basics;

public class ExceptionHandles {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		String strNum = "10";
		try{
			int num = Integer.parseInt(strNum);
			System.out.println("You entered: " + num);
		}
		catch(NumberFormatException e)
		{
			System.out.println("Integer number required.." + e);
		}
		
		
	}

}
