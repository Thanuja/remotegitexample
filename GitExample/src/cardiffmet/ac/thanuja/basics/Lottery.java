package cardiffmet.ac.thanuja.basics;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Lottery {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		int[] lotteryNumbers = new int[6];
		int MIN = 1;
		int MAX = 42;
		
		System.out.println("How many birthdays, would like to include? ");
		Scanner input = new Scanner(System.in);
		int numBdays = input.nextInt();
		if (numBdays > 6)
		{
			System.out.println("Maximum total numbers should be <= 6");
			System.exit(1);
		}
		
		int counter = 0;
		while(counter < numBdays)
		{
			System.out.println("Pls. enter a bday: ");
			int num = input.nextInt();
			lotteryNumbers[counter] = num;
			counter++;
		}
		
		for(int i = counter-1; i < 6; i++)
			lotteryNumbers[i] = getRandomNumber(MIN, MAX);	
		
	      // sorting array
	      //Arrays.sort(lotteryNumbers);
		int[] sortedArray = bubbleSort(lotteryNumbers);
	      
		for(int num:sortedArray)
			System.out.println(num);
	      
	}
	
	public static int getRandomNumber(int min, int max)
	{
		int randomNumber;
		// option 1:
		/*
		 * Random().nextInt(int bound) generates a random integer from 0 (inclusive) 
		 * to bound (exclusive).
		 * 
		 *  max-min = range
		 * (max-min) + 1 = includes the last value
		 *  rand((max-min) + 1) + min = last +min gives the start value..
		 */
				
		//Random r = new Random();
		//randomNumber = r.nextInt((max - min) + 1) + min; // this gave similar numbers??
	
		// option 2:
		/*
		 * Math.random() gives a random number between 0 and 1
		 * 
		 */
		randomNumber = (int)(Math.random() * ((max - min) + 1)) + min;
		return randomNumber;
		
	}
	
	public static int[] bubbleSort(int arr[]) 
    { 
        int n = arr.length; 
        for (int i = 0; i < n-1; i++) 
        {
            for (int j = 0; j < n-i-1; j++) 
            {
                if (arr[j] > arr[j+1]) 
                { 
                    // swap temp and arr[i] 
                    int temp = arr[j]; 
                    arr[j] = arr[j+1]; 
                    arr[j+1] = temp; 
                }             	
            	
            }        	
        }
        return arr;


    }
	

}
