package cardiffmet.ac.thanuja.basics;

public class Loops {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		for(int i = 0; i< 11; i++)
		{
			System.out.println("For Count: " + i);
		}
		
		int counter = 0;
		while(counter < 10)
		{
			System.out.println("While Count: " + counter);
			counter++;
		}
		
		counter  = 0;
		do
		{
			System.out.println("Do While Count: " + counter);
			counter++;
		}
		while(counter < 11);
		// When would you use, do while and While??
		// loop runs onces in any case, even though the condition is never true
		
		
		
		// Nested loops, where will you need nested loops???
		int loop_counter = 0;
		for (int i =0; i < 5; i++)
		{
			System.out.println("Outer loop: " + i);
			for (int j = 0; j < 10; j++)
			{
				System.out.println("\t\tInner loop: " + j + " , total number: " + (++loop_counter));
			}
		}
		
		//infinite for loop
//		for(;;)
//		{
//			
//		}
		
		// for each loops; enhanced for loops
		int[] numbers = {1,2,3,4,5,6,7,8,9,10};
		for(int item: numbers)
		{
			System.out.println("Count is: " + item);
		}
		
		
		/*
		 * Break statements; labelled, unlabeld
		 * 
		 */
		// Break from for loops, unlabeled, breaks an inner for loop
		int[] arrayOfInts = { 32, 87, 3, 589, 12, 1076, 2000, 8, 622, 127 };
        int searchfor = 12;
        int i;
        boolean foundIt = false;
        for (i = 0; i < arrayOfInts.length; i++) 
        {
            if (arrayOfInts[i] == searchfor) 
            {
                foundIt = true;
                break;
            }
        }
        if (foundIt) 
        {
            System.out.println("Found " + searchfor + " at index " + i);
        } else {
            System.out.println(searchfor + " not in the array");
        }
        
        // labeled breaks, break the outer loops, in nested loops for 2D arrays
        int[][] arrayOfInts2D = { { 32, 87, 3, 589 }, { 12, 1076, 2000, 8 }, { 622, 127, 77, 955 } };
        searchfor = 12;
        int ii;
        int jj = 0;
        foundIt = false;

        //label
        search:
	        for (ii = 0; ii < arrayOfInts2D.length; ii++) {
	            for (jj = 0; jj < arrayOfInts2D[ii].length; jj++) {
	                if (arrayOfInts2D[ii][jj] == searchfor) {
	                    foundIt = true;
	                    break search;
	                }
	            }
	        }

        if (foundIt) {
            System.out.println("Found " + searchfor + " at " + ii + ", " + jj);
        } else {
            System.out.println(searchfor + " not in the array");
        }
        
        /*
         * continure statement; labeled and unlabeled.
         */
        //	unlabeld, skip innter loop iteration 
        // 	Example; count the number of p characters in the string; 
        String searchMe = "peter piper picked a " + "peck of pickled peppers";
        int max = searchMe.length();
        int numPs = 0;

        for (int c = 0; c < max; c++) 
        {
            // interested only in p's
            if (searchMe.charAt(c) != 'p')
                continue;
            // process p's
            numPs++;
        }
        System.out.println("Found " + numPs + " p's in the string.");
		
		// labeld: skip an interation in the outermost loop
        // see the example: https://docs.oracle.com/javase/tutorial/java/nutsandbolts/branch.html
        
        /*
         * return statement
         * with a value, without a value
         * without a value is used to exit from a method/function, when the method return type
         * is declared as void
         */
        
        
        
        
        
        
	}

}
