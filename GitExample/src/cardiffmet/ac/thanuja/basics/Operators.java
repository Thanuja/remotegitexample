package cardiffmet.ac.thanuja.basics;

/**
 * 
 * @author thanu
 *	Operators: Arithmetic, Assignment, COmparison
 *	Arithmetic:
 *		+, -, *, /, % (modulus), ++, --
 *	Assignment
 *		=, +=, -=, *=, /=, %= (assigns the remainder)
 *	comparison
 *		==, != , >, >=, <, <=
 *	
 *	It is important to know the precedance of the operators
 *	operators with high precedence are evaluated first below the ones with lower precedence
 *	Assignment operators are evaluated RIght to Left
 *	Other: Left to Right
 *	Order
 *	1. x++, x--, 
 *	2. ++x, +x, -y 
 *	3. *, /, %
 *	4, +, -
 *	5. <<, >>, >>> Unsinged shift
 *	6. < >, >=, <=
 *	7. ==, !=
 *	8. & - bitwise and
 *	9, ^ - exclusive OR
 *	10. | - inclusive OR
 *	11. && - Logical AND
 *	12. ||
 *	13. ? : ternary
 *	14. assignment: =, +=, ...etc
 *
 */
public class Operators {

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		int num = 100;
		int factor = 20;
		int sum = 0;
		
		sum = num + factor;
		System.out.println("The sum of " + num + " and " + factor + " is: " + sum);
		
		// Add -, *, / and % operations
		// Why do you get a yellow bulb when you declare a variable??
		
		// string concatenation
		String firstString = "This is";
        String secondString = " a concatenated string.";
        String thirdString = firstString+secondString;
        System.out.println(thirdString);
        
        // Unary operators: Need only one operand; +, -, ++, --, !
        int result = +1;
        result --;
        result++;
        boolean success = false;
        if(result>0)
        	success = !success;
        
        // difference between prefix and postfix values; ++result and result++
        result = 3;
        System.out.println("++Result: " + ++result);
        System.out.println("Now Result: " + result);
        System.out.println("Result++: " + result++);
        System.out.println("Now Result: " + result);
        
        // if then else operator; ? :
        // What is the name of this?? Ternary operator, takes 3 arguments
        int value1 = 1;
        int value2 = 2;
        boolean someCondition = true;
        result = someCondition ? value1 : value2;
        
        //Instance of operator..
		/*
		 * Compares objects to a specific type
		 * 
		 */
		
        String school = "School of Technologies";
        System.out.println("Is school an instance of String : " + (school instanceof String));
        
        /*
         * Bitwise and bitshift operators
         * 
         */
        
        byte startcode = 0b0000111;
        byte inverted_startcode = (byte) ~startcode; // check the 2's complement value and convert to decimal
        System.out.println("Inversted start code: " + inverted_startcode);
        
        byte leftshifted_startcode = (byte) (startcode << 2);
        System.out.println("Left shifted start code: " + leftshifted_startcode);
        byte rightshifted_startcode = (byte) (startcode >> 2);
        System.out.println("Right shifted start code: " + rightshifted_startcode);
        
        int bitmask = 0x000f;
        int values = 0x2222;
        // Bitwise AND operation
        System.out.println(values & bitmask);
        
        
        
        
        
        
        
        
        
	}

}
