package cardiffmet.ac.thanuja.basics;

public class SwitchPractice {

	/**
	 * @param args
	 * @return 
	 * 
	 * Switch vs. if else then,..
	 * 
	 * decided based on the readability and type of the expressions
	 * switch(x);
	 * 		x can be an integer, string, or enum value
	 * 		when using strings, better to bring it a single case, avoids complications
	 * 			e.g, Stringname.tolowercase(), or name.touppercase()
	 * 		if a string in null, NullPointerException is thrown!!
	 * 
	 * if (expression)
	 * 		can be a more complex expression with multiple conditions
	 * 
	 * break statement:
	 * 		without break, all case statements will be executed
	 * 
	 * 
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int month = -1;
		if (args.length > 0)
			month = Integer.parseInt(args[0]);
		else{
			System.out.println("No month is provided!!");
			System.exit(1);
		}
		
		switch(month)
		{
			case 1:
				System.out.println("Month chosen in January!");
				break;
			case 2:
				System.out.println("Month chosen in February!");
				break;
			case 3:
				System.out.println("Month chosen in March!");
				break;
			case 4:
				System.out.println("Month chosen in April!");
				break;
			case 5:
				System.out.println("Month chosen in May!");
				break;
			case 6:
				System.out.println("Month chosen in June!");
				break;
			case 7:
				System.out.println("Month chosen in July!");
				break;
			case 8:
				System.out.println("Month chosen in August!");
				break;
			case 9:
				System.out.println("Month chosen in September!");
				break;
			case 10:
				System.out.println("Month chosen in October!");
				break;
			case 11:
				System.out.println("Month chosen in November!");
				break;
			case 12:
				System.out.println("Month chosen in December!");
				break;
				
			default:
				System.out.println("Invalid Month chosen!");
				break;
		}
	
		
		//show the seasons
		switch(month)
		{
			case 1: case 2: case 12:
				System.out.println("The season is Winter");
				break;
				
			case 3: case 4: case 5:
				System.out.println("The season is Spring");
				break;
				
			case 6: case 7: case 8:
				System.out.println("The season is Summer");
				break;
				
			case 9: case 10: case 11:
				System.out.println("The season is Autum");
				break;			
				
			default:
				System.out.println("Invalid Month chosen!");
				break;
		}
		
		
		
	}

}
